# libftasm - @WeThinkCode_ 
)
The libasm project’s goal is to make you code a small library in ASM. You have to
recode some of the basic libc function to be able to generate a library. At the end of this
project you should be familiar with the language’s syntax, the stack’s operating as well
as the compilator’s behavior.

### What's in it?

1.  **Libc Functions:** Some of the standard C functions

Libc functions | 
:----------- | 
bzero		|
strcat		|  
isalpha		|     
isdigit		|   
isalnum		| 
isascii		| 
isprint		| 
toupper		| 
tolower		| 
puts		| 
strlen		| 
memset		| 
memcpy		| 
strdup		| 

# General Instructions
1) The library must be called libfts.a.
2) You must compile your assembly code with nasm.
3) You must write 64 bits ASM.
4) You can’t do inline ASM, you must do ’.s’ files.
6) You must use the Intel syntax, not the AT&T.
7) You must submit a main that will test your functions and that will compile with
your library to show that it’s functional.

# How to run the tests
```shell
$> make 
$> make run
```

